# Introdaction
For printful test project NextJs + React was used. In this bundle, NextJs is used as a ready-made NodeJs server, which is equipped with ExpressJs and, if necessary, makes it easy to interfere with NextJs. Such an intervention allows you to install for example SocketIo, MongoDB, or other modules necessary for the server side. As result of NextJs using, I have M.E.R.N. stack, but I dont want to use MongoDB and custom ExpressJs for project.

In this case, I had to abandon some technologies (TypeScript and EsLint), my home Windows PC does not want to use them correctly (my main work station is Mac). Most likely this can be solved, but at the moment these efforts are not needed.

* Prop-types are used for React Component props typing
* Less as style manager
* Antd desing is used as an analog bootstrap


## Testing
* https://printful.now.sh/
* https://printful-d03ynzp64.now.sh/
* https://printful.muvvekk.now.sh/

## Atomic design pattern
This pattern provides for sorting components by size. In my case, the structure looks like this:
* layouts/atoms
* layouts/molecules
* layouts/organisms
* layouts/templates
* pages (NextJS uses this folder as a router)

### Atoms
Basic building blocks of matter, such as a button, input or a form label. They’re not useful on their own.

### Molecules
Grouping atoms together, such as combining a button, input and form label to build functionality.

### Organisms
Combining molecules together to form organisms that make up a distinct section of an interface (i.e. navigation bar)

### Templates
Consisting mostly of groups of organisms to form a page — where clients can see a final design in place.

### Pages
An ecosystem that views different template renders. We can create multiple ecosystems into a single environment — the application.


## Installation
### Development
```bash
npm i
npm run dev
```

### Production
```bash
npm i --production
npm run build
npm run start
```
