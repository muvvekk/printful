import PropTypes from 'prop-types';
import classNames from 'classnames';

const WrapperMiddle = ({ children, full }) => {
  const componentClass = classNames({
    'wrapper-middle': true,
    'wrapper-middle-full': full
  });

  return <div className={componentClass}>{children}</div>;
};

export default WrapperMiddle;

WrapperMiddle.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.array,
    PropTypes.elementType
  ]).isRequired,
  full: PropTypes.bool
};

WrapperMiddle.defaultProps = {
  full: false
};
