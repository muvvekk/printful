import PropTypes from 'prop-types';
import Router from 'next/router';
import { Form } from 'antd';

import FieldUserName from '@molecules/fields/UserName';
import FieldTestSelection from '@molecules/fields/TestSelection';

import FormSubmit from '@molecules/forms/Submit';

class FormTestSelection extends React.Component {
  constructor() {
    super();

    this.state = {
      loading: false,
      userName: ''
    };
  }

  componentDidMount() {
    const userName = localStorage.getItem('userName');

    if (userName) {
      this.setState({ userName });
    }
  }

  handleSubmit = event => {
    event.preventDefault();

    const { form } = this.props;

    form.validateFields((errors, { name, test_id }) => {
      if (!errors) {
        if (name) {
          localStorage.setItem('userName', name);
        }

        this.setState({ loading: true });
        Router.push('/[name]/[test_id]/[question-id]', `/${name}/${test_id}/0`);
      }
    });
  };

  render() {
    const { loading, userName } = this.state;
    const { form, tests } = this.props;

    return (
      <Form onSubmit={this.handleSubmit}>
        <FieldUserName form={form} value={userName} required />
        <FieldTestSelection form={form} tests={tests} required />

        <FormSubmit loading={loading} text="Start test" />
      </Form>
    );
  }
}

export default Form.create({ name: 'test-selection' })(FormTestSelection);

FormTestSelection.propTypes = {
  form: PropTypes.shape({}).isRequired,
  tests: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired
    }).isRequired
  ).isRequired
};
