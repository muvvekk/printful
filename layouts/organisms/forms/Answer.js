import PropTypes from 'prop-types';
import Router from 'next/router';
import Cookies from 'js-cookie';
import { Form } from 'antd';

import FieldAnswerOptions from '@molecules/fields/AnswerOptions';
import FormSubmit from '@molecules/forms/Submit';

class FormAnswer extends React.Component {
  constructor() {
    super();

    this.state = {
      loading: false
    };
  }

  handleCookieUpdate = answer => {
    const { test_id, nextQuestionId } = this.props;

    const cookieName = `test-${test_id}`;
    const cookieAnswers = Cookies.get(cookieName);

    let data = [answer];

    if (cookieAnswers && nextQuestionId !== 1) {
      data = data.concat(JSON.parse(cookieAnswers));
    }

    Cookies.set(cookieName, JSON.stringify(data));
  };

  handleSubmit = event => {
    event.preventDefault();

    const { form, name, test_id, nextQuestionId } = this.props;

    form.validateFields((errors, { answer }) => {
      if (!errors) {
        this.setState({ loading: true });
        this.handleCookieUpdate(answer);

        Router.push(
          '/[name]/[test_id]/[question-id]',
          `/${name}/${test_id}/${nextQuestionId}`
        );
      }
    });
  };

  render() {
    const { question, form, answers } = this.props;
    const { nextId, loading } = this.state;

    const buttonText = nextId === 'result' ? 'Result' : 'Next question';

    return (
      <Form onSubmit={this.handleSubmit}>
        <FieldAnswerOptions
          form={form}
          options={answers}
          label={question.title}
          required
        />
        <FormSubmit loading={loading} text={buttonText} />
      </Form>
    );
  }
}

export default Form.create({ name: 'test-answer' })(FormAnswer);

FormAnswer.propTypes = {
  form: PropTypes.shape({}).isRequired,
  test_id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  question: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired
  }).isRequired,
  answers: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  nextQuestionId: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    .isRequired
};
