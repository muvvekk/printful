import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import { Result } from 'antd';

const AnswerBlockResult = ({ result }) => {
  const router = useRouter();
  const { name } = router.query;

  return (
    <Result
      status="success"
      title="Successfully!"
      subTitle={`${name}, you correctly answered ${result.correct} of ${result.total} questions`}
    />
  );
};

export default AnswerBlockResult;

AnswerBlockResult.propTypes = {
  result: PropTypes.shape({
    correct: PropTypes.number.isRequired,
    total: PropTypes.number.isRequired
  }).isRequired
};
