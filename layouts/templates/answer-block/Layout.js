import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import { Progress } from 'antd';

import FormAnswer from '@organisms/forms/Answer';

const AnswerBlockLayout = ({ questions, answers, nextQuestionId }) => {
  const router = useRouter();
  const { name, test_id, question_id } = router.query;

  const questionIndex = parseInt(question_id, 0);
  const questionsCount = questions.length;
  const currentProgress = (questionIndex / questionsCount) * 100;

  return [
    <FormAnswer
      key="form"
      name={name}
      question={questions[question_id]}
      answers={answers}
      test_id={test_id}
      nextQuestionId={nextQuestionId}
    />,
    <Progress key="progress" percent={currentProgress} showInfo={false} />
  ];
};

export default AnswerBlockLayout;

AnswerBlockLayout.propTypes = {
  questions: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired
    }).isRequired
  ),
  answers: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired
    }).isRequired
  ),
  nextQuestionId: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    .isRequired
};

AnswerBlockLayout.defaultProps = {
  questions: [],
  answers: []
};
