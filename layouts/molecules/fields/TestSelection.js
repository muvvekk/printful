import PropTypes from 'prop-types';
import { Form, Select } from 'antd';

class FieldTestSelection extends React.Component {
  render() {
    const { value, required, form, tests } = this.props;

    return (
      <Form.Item label="Choose test">
        {form.getFieldDecorator('test_id', {
          initialValue: value,
          rules: [{ required: required, message: 'Please select test!' }]
        })(
          <Select>
            {tests.map(({ title, id }) => (
              <Select.Option key={id} value={id}>
                {title}
              </Select.Option>
            ))}
          </Select>
        )}
      </Form.Item>
    );
  }
}

export default FieldTestSelection;

FieldTestSelection.propTypes = {
  value: PropTypes.string,
  required: PropTypes.bool,
  form: PropTypes.shape({
    getFieldDecorator: PropTypes.func.isRequired
  }).isRequired,
  tests: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired
    }).isRequired
  ).isRequired
};

FieldTestSelection.defaultProps = {
  value: '',
  required: false
};
