import PropTypes from 'prop-types';
import { Form, Radio } from 'antd';

const FieldAnswerOptions = ({ options, required, form, label }) => (
  <Form.Item label={label}>
    {form.getFieldDecorator('answer', {
      rules: [{ required: required, message: 'Please select answer!' }]
    })(
      <Radio.Group size="large" className="answer-group">
        {options.map(({ title, id }) => (
          <Radio key={id} value={id}>
            {title}
          </Radio>
        ))}
      </Radio.Group>
    )}
  </Form.Item>
);

export default FieldAnswerOptions;

FieldAnswerOptions.propTypes = {
  required: PropTypes.bool,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      id: PropTypes.number.isRequired
    }).isRequired
  ).isRequired,
  form: PropTypes.shape({
    getFieldDecorator: PropTypes.func.isRequired
  }).isRequired,
  label: PropTypes.string
};

FieldAnswerOptions.defaultProps = {
  required: false,
  label: ''
};
