import PropTypes from 'prop-types';
import { Form, Input } from 'antd';

const FieldTestSelection = ({ value, required, form }) => (
  <Form.Item label="Enter your name">
    {form.getFieldDecorator('name', {
      initialValue: value,
      rules: [{ required: required, message: 'Please enter your name!' }]
    })(<Input />)}
  </Form.Item>
);

export default FieldTestSelection;

FieldTestSelection.propTypes = {
  value: PropTypes.string,
  required: PropTypes.bool,
  form: PropTypes.shape({
    getFieldDecorator: PropTypes.func.isRequired
  }).isRequired
};

FieldTestSelection.defaultProps = {
  value: '',
  required: false
};
