import PropTypes from 'prop-types';
import { Row, Col, Button } from 'antd';

const FormSubmit = ({ loading, text }) => (
  <Row type="flex" justify="end">
    <Col>
      <Button htmlType="submit" type="primary" loading={loading}>
        {text}
      </Button>
    </Col>
  </Row>
);

export default FormSubmit;

FormSubmit.propTypes = {
  loading: PropTypes.bool,
  text: PropTypes.string.isRequired
};

FormSubmit.defaultProps = {
  loading: false
};
