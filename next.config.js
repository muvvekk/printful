const path = require('path');

const withLess = require('@zeit/next-less');

const config = {
  cssModules: false,
  lessLoaderOptions: {
    javascriptEnabled: true
  },
  webpack: config => {
    config.plugins = config.plugins || [];

    config.resolve.alias = {
      ...config.resolve.alias,
      '@utils': path.resolve(__dirname, 'utils'),
      '@styles': path.resolve(__dirname, 'styles'),
      '@layouts': path.resolve(__dirname, 'layouts'),
      '@atoms': path.resolve(__dirname, 'layouts/atoms'),
      '@molecules': path.resolve(__dirname, 'layouts/molecules'),
      '@organisms': path.resolve(__dirname, 'layouts/organisms'),
      '@templates': path.resolve(__dirname, 'layouts/templates')
    };

    return config;
  }
};

module.exports = withLess(config);
