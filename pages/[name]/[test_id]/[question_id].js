import PropTypes from 'prop-types';
import fetch from 'isomorphic-unfetch';
import { Card } from 'antd';

import services from '@utils/services.js';

import WrapperMiddle from '@organisms/wrappers/Middle';
import AnswerBlockLayout from '@templates/answer-block/Layout';

const QuestionPage = ({ questions, answers, nextQuestionId }) => (
  <WrapperMiddle full>
    <Card
      bordered={false}
      style={{ width: '100%', maxWidth: 600, minWidth: 300 }}
    >
      <AnswerBlockLayout
        questions={questions}
        answers={answers}
        nextQuestionId={nextQuestionId}
      />
    </Card>
  </WrapperMiddle>
);

QuestionPage.getInitialProps = async ({ query }) => {
  const { test_id, question_id } = query;
  const localId = parseInt(question_id);

  const questionsRes = await fetch(
    `${services.testQuiz}?action=questions&quizId=${test_id}`
  );
  const questionsData = await questionsRes.json();
  const questionId = questionsData[question_id].id;

  const answersRes = await fetch(
    `${services.testQuiz}?action=answers&quizId=${test_id}&questionId=${questionId}`
  );
  const answersData = await answersRes.json();

  const nextQuestionId = questionsData[localId + 1] ? localId + 1 : 'result';

  return { questions: questionsData, answers: answersData, nextQuestionId };
};

export default QuestionPage;

QuestionPage.propTypes = {
  questions: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired
    }).isRequired
  ),
  answers: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired
    }).isRequired
  ),
  nextQuestionId: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    .isRequired
};

QuestionPage.defaultProps = {
  questions: [],
  answers: []
};
