import nextCookies from 'next-cookies';
import PropTypes from 'prop-types';
import fetch from 'isomorphic-unfetch';
import { Card } from 'antd';

import services from '@utils/services.js';

import WrapperMiddle from '@organisms/wrappers/Middle';
import AnswerBlockResult from '@templates/answer-block/Result';

const ResultPage = ({ result }) => (
  <WrapperMiddle full>
    <Card bordered={false} style={{ width: 300 }}>
      <AnswerBlockResult result={result} />
    </Card>
  </WrapperMiddle>
);

ResultPage.getInitialProps = async ctx => {
  const { test_id } = ctx.query;
  const cookies = nextCookies(ctx);
  const answers = cookies[`test-${test_id}`] || [];

  const answersQuery = answers.map(value => `answers[]=${value}`).join('&');

  const resultRes = await fetch(
    `${services.testQuiz}?action=submit&quizId=${test_id}&${answersQuery}`
  );
  const resultData = await resultRes.json();

  return { result: resultData };
};

export default ResultPage;

ResultPage.propTypes = {
  result: PropTypes.shape({
    correct: PropTypes.number.isRequired,
    total: PropTypes.number.isRequired
  }).isRequired
};
