import PropTypes from 'prop-types';
import fetch from 'isomorphic-unfetch';
import { Card } from 'antd';

import services from '@utils/services.js';

import WrapperMiddle from '@organisms/wrappers/Middle';
import FormTestSelection from '@organisms/forms/TestSelection';

const HomePage = ({ tests }) => (
  <WrapperMiddle full>
    <Card bordered={false} style={{ width: 300 }}>
      <FormTestSelection tests={tests} />
    </Card>
  </WrapperMiddle>
);

HomePage.getInitialProps = async () => {
  const res = await fetch(`${services.testQuiz}?action=quizzes`);
  const tests = await res.json();

  return { tests };
};

export default HomePage;

HomePage.propTypes = {
  tests: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired
    }).isRequired
  )
};

HomePage.defaultProps = {
  tests: []
};
