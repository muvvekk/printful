import PropTypes from 'prop-types';

import '@styles/styles.less';

const MyApp = ({ Component, pageProps }) => {
  return <Component {...pageProps} />;
};

export default MyApp;

MyApp.propTypes = {
  pageProps: PropTypes.object.isRequired,
  Component: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.array,
    PropTypes.elementType
  ]).isRequired
};
